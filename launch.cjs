const puppeteer = require("puppeteer");

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  // Googlebot User-Agent 설정
  await page.setUserAgent(
    "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
  );

  // 로컬 웹사이트 열기
  await page.goto("https://www.yeoshin.co.kr/event/mobile/1785", {
    waitUntil: "networkidle2",
  });

  // 페이지 콘텐츠 가져오기
  const content = await page.content();
  console.log(content);

  await browser.close();
})();
